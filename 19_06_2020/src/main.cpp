#include <Arduino.h>

#define SENSOR = 2;
#define LED = 13;

#define estado = LOW;

void setup() {

pinMode(LED, OUTPUT);
pinMode(SENSOR, INPUT);

attachInterrupt(digitalPinToInterrupt(SENSOR), Interrupcion, FALLING);

digitalWrite(LED, estado);

}

void loop() 
{
  digitalWrite(LED, estado);
}

void Interrupcion (void)

{
  estado = !estado;
}
